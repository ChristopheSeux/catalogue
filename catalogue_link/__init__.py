import bpy, threading, os, time, json, socket
import bpy.utils.previews
from pathlib import Path
from bpy.app.handlers import persistent
#import template
#from PySide2.QtCore import QThread
import pickle



def load_asset(datas) :
    print('Load asset', datas)
    is_relative = bpy.context.preferences.filepaths.use_relative_paths
    scene = bpy.context.scene

    for data in datas :
        if data['type'] == 'textures' :

            ob = bpy.context.view_layer.objects.active
            if not ob :
                print('No active Object')
                return

            mat = ob.active_material
            if not mat :
                print('No Active Material')
                return

            mat.use_nodes = True
            for i, f in enumerate(data['files']) :
                tex_node = mat.node_tree.nodes.new('ShaderNodeTexImage')
                tex_node.location= (-600, i*300)

                tex_image = bpy.data.images.load(f, check_existing=True)
                if is_relative and bpy.data.filepath :
                    tex_image.filepath = bpy.path.relpath(tex_image.filepath)

                tex_node.image = tex_image

                name = Path(f).name.lower()
                is_color =  any([i in name for i in ('diff', 'color', 'albedo')])
                if scene.display_settings.display_device == 'sRGB' :
                    tex_image.colorspace_settings.name = 'sRGB' if is_color else 'Non-Color'

                elif scene.display_settings.display_device == 'ACES' :
                    tex_image.colorspace_settings.name = 'Utility - sRGB - Texture' if is_color else 'Utility - Raw'


        if data['type'] == 'hdri' :
            world = scene.world
            if not world :
                world = bpy.data.world.new('World')
                scene.world = world

            #remove all node from the active world
            for node in world.node_tree.nodes :
                world.node_tree.nodes.remove(node)

            env_node = world.node_tree.nodes.new('ShaderNodeTexEnvironment')
            env_tex = bpy.data.images.load(data['files'][0], check_existing=True)
            if is_relative and bpy.data.filepath :
                env_tex.filepath = bpy.path.relpath(env_tex.filepath)

            if scene.display_settings.display_device == 'sRGB' :
                env_tex.colorspace_settings.name = 'Linear'

            elif scene.display_settings.display_device == 'ACES' :
                env_tex.colorspace_settings.name = 'Utility - Linear - sRGB'

            env_node.image = env_tex

            mapping_node = world.node_tree.nodes.new('ShaderNodeMapping')
            mapping_node.location = (-300, 0)
            mapping_node.vector_type = 'TEXTURE'

            tex_co_node = world.node_tree.nodes.new('ShaderNodeTexCoord')
            tex_co_node.location = (-500, 0)

            background_node = world.node_tree.nodes.new('ShaderNodeBackground')
            background_node.location = (400, 0)

            output_node = world.node_tree.nodes.new('ShaderNodeOutputWorld')
            output_node.location = (600, 0)

            world.node_tree.links.new(tex_co_node.outputs['Generated'], mapping_node.inputs['Vector'])
            world.node_tree.links.new(mapping_node.outputs['Vector'], env_node.inputs['Vector'])
            world.node_tree.links.new(env_node.outputs['Color'], background_node.inputs['Color'])
            world.node_tree.links.new(background_node.outputs['Background'], output_node.inputs['Surface'])


'''
def get_task_items(self, context) :
    dlp = context.window_manager.doliprane
    return [(t, t, '') for t in dlp.data['task_items']]

def get_status_items(self, context) :
    dlp = context.window_manager.doliprane
    items = dlp.data['all_status_items']
    if dlp.artist_allowed_only :
        items = dlp.data['artist_status_items']
    return [(s, s, '') for s in items]


class Properties(bpy.types.PropertyGroup):
    """Class with all properties saved by blender for this project"""

    #screenshot : bpy.props.StringProperty(name="Screenshot")
    #entity_type : bpy.props.StringProperty(name="Entity Type")
    #project_name : bpy.props.StringProperty(name="Project name")
    #template : bpy.props.EnumProperty(items = [])
    artist_allowed_only = bpy.props.BoolProperty(name='Artist Allowed Only', default = True)
    comment : bpy.props.StringProperty(name='Comment')
    playblast : bpy.props.BoolProperty(name='Make Playblast')
    status : bpy.props.EnumProperty(items=get_status_items, name='Status')
    task : bpy.props.EnumProperty(items=get_task_items,name = 'Task')

    data = {
        'screenshot' : '',
        'task_items' : [],
        'all_status_items' : [],
        'artist_status_items' : []
    }
'''



class OBJECT_OT_connect_to_catalogue(bpy.types.Operator):
    """Check if the file is conform to the pipeline"""

    bl_idname = "object.connect_to_catalogue"
    bl_label = "Sanity Check"

    def execute(self, context):
        print('Op connect to catalogue')
        return {"FINISHED"}


'''
class TOPBAR_MT_catalogue_menu(bpy.types.Menu):
    """Class to draw the menu"""

    bl_idname = "TOPBAR_MT_pm_menu"
    bl_label = "Meat Dept"

    def draw(self, context):
        prefs = context.window_manager.doliprane
        layout = self.layout
        layout.operator("object.connect_to_catalogue", icon_value=prefs.icons['increment'].icon_id)
'''

class ClientBase(threading.Thread) :
    def __init__(self) :
        super().__init__()

        self.host = socket.gethostname()
        self.port = 9999
        self.is_running = True
        self.buffer_size = 4096
        self.is_connected = False

        try :
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server.connect((self.host, self.port))
            self.is_connected = True
        except :
            print('[CLIENT] No server to connect')
        #self.server.setblocking(0)

    def send(self, message) :
        if self.is_connected :
            msg = pickle.dumps(message)
            self.server.sendall(msg)
            print('[CLIENT] Message sent :', message)

    def receive(self) :
        msg = b''
        while self.is_running :
            try :
                print('[CLIENT] Waiting for server input')
                chunk = self.server.recv(self.buffer_size)
            except Exception as e :
                self.is_running = False
                break

            msg+= chunk
            if not chunk or len(chunk) < self.buffer_size: break

        if msg :
            return pickle.loads(msg)

    def close(self) :
        print('[CLIENT] Client is asking the server to close the connection')
        self.send('Exit')
        self.is_running = False
        #self.server.shutdown(2)
        self.server.close()



class ThreadChecker(threading.Thread):
    def __init__(self, parent) :
        super().__init__()
        self.parent = parent
        self.is_running = True

    def run(self):
        while self.is_running :
            time.sleep(2)
            for i in threading.enumerate():
                if(i.getName() == "MainThread" and i.is_alive() == False):
                    self.close()
                    break

        print('[CLIENT] Checker Thread terminate')

    def close(self) :
        self.parent.close()
        self.is_running = False


class ClientThread(ClientBase):
    def __init__(self) :
        super().__init__()
        self.thread_checker = ThreadChecker(self)
        self.thread_checker.start()

        print('[CLIENT] Start Client Thread')

    def run(self):
        while self.is_running :
            msg = self.receive()

            if not msg : break

            print('[CLIENT] Data Receive from server :', msg)

            if isinstance(msg, (list, tuple)) and len(msg) == 2 :
                if msg[0] == 'GET' :
                    try :
                        self.send(['POST', {k: eval(v) for k,v in msg[1].items() }] )
                    except Exception as e :
                        print()

                elif msg[0] == 'POST' :

                    load_asset(msg[1])
                    #data = bpy.context.window_manager.doliprane.data
                    #data.update(msg[1])
                    #setattr(bpy.context.window_manager.doliprane, data[1], data[2])


            if msg == 'Exit' :
                print('Disconnect Socket')
                self.close()

        print('[CLIENT] Thread terminate')


FILEPATH = None


@persistent
def send_filepath(scene=None):
    print('[CLIENT] Start Client Handler')

    global FILEPATH

    if bpy.data.filepath != FILEPATH :
        client = bpy.context.window_manager.client
        client.send(['POST',{'filepath': bpy.data.filepath} ] )

    FILEPATH = bpy.data.filepath



def draw_item(self, context):
    """draw the menu"""

    layout = self.layout
    layout.menu(TOPBAR_MT_meat_dept_menu.bl_idname)


classes = [
]


def register():
    if not hasattr(bpy.types.WindowManager, 'client') :
        bpy.types.WindowManager.client = ClientThread()
        bpy.types.WindowManager.client.start()

    if send_filepath not in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.append(send_filepath)

    if send_filepath not in bpy.app.handlers.save_post:
        bpy.app.handlers.save_post.append(send_filepath)

    for cls in classes:
        bpy.utils.register_class(cls)

    #Properties and icons
    #bpy.types.WindowManager.doliprane = bpy.props.PointerProperty(type=Properties)
    #pcoll = bpy.utils.previews.new()
    #for icon in (Path(__file__).parent/'icons').glob('*.png') :
    #    pcoll.load(icon.stem, icon.as_posix(), 'IMAGE')
    #Properties.icons = pcoll

    #bpy.types.TOPBAR_MT_editor_menus.append(draw_item)

def unregister():
    for cls in classes[::-1]:
        bpy.utils.unregister_class(cls)

    if send_filepath in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.remove(send_filepath)

    if send_filepath in bpy.app.handlers.save_post:
        bpy.app.handlers.save_post.remove(send_filepath)

    #bpy.utils.previews.remove(Properties.icons)
    #del bpy.types.WindowManager.doliprane

#if __name__ == '__main__':
#    start_client()
