from PySide2.QtWidgets import QApplication
from pathlib import Path
import yaml

MODULE_DIR = Path(__file__).parent

ICON_DIR = MODULE_DIR/'resources'/'icons'
STYLESHEET = (MODULE_DIR/'stylesheet.css').read_text()
CONFIG = yaml.safe_load( (MODULE_DIR/'config.yml').read_text() )
APP = QApplication.instance()
