from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

from catalogue.constants import ICON_DIR
from pathlib import Path


class AbstractWidget:
    def __init__(self, parent=None, cursor=None, tag=None, props={}):
        if tag :
            self.setObjectName(tag)
        if cursor :
            self.setCursor(getattr(Qt, cursor))

        # Default Properties
        props = {
            'FontWeight':'light',
            'FontSize':'normal',
            'Height':'none',
            'Rounded':'none',
            **props
            }

        if 'Margin' in props :
            props['Vmargin'] = props['Margin']
            props['Hmargin'] = props.pop('Margin')

        if 'Padding' in props :
            props['Vpadding'] = props['Padding']
            props['Hpadding'] = props.pop('Padding')

        if 'Size' in props :
            props['Width'] = props['Size']
            props['Height'] = props.pop('Size')

        for k, v in props.items() :
            self.setProperty(k, v)

        layout = parent
        if hasattr(parent,'layout') :
            layout = parent.layout
            if callable(parent.layout) :
                layout = parent.layout()

        if layout :
            layout.addWidget(self)


class AbstractLayout :
    def __init__(self, parent=None, spacing=0, margin=0, align=None,
    orientation=None, props={}):

        for k, v in props.items() :
            self.setProperty(k, v)

        self.setSpacing(spacing)

        if align :
            if isinstance(align, (tuple, list) ) :
                align = getattr(Qt, 'Align'+align[0]) | getattr(Qt, 'Align'+align[1])
            else :
                align = getattr(Qt, 'Align'+align)

            self.setAlignment(align)

        if orientation :
            self.setOrientation(getattr(Qt, orientation) )

        #Margin
        if isinstance(margin, (float, int)) :
            margin = [margin]*4
        self.setContentsMargins(*margin)

        #Parent
        if isinstance(parent, QLayout) :
            parent.addLayout(self)
        elif parent :
            parent.setLayout(self)


class Layout(QBoxLayout):
    def __init__(self, parent=None, direction='LeftToRight', spacing=0,
    margin=0, align=None, **kargs):
        QBoxLayout.__init__(self, getattr(QBoxLayout, direction))
        AbstractLayout.__init__(self, parent, spacing, margin, align, props=kargs)

    def clear(self) :
        for i in reversed(range(self.count())):
            item = self.takeAt(i)
            if hasattr(item, 'widget') :
                widget = item.widget
                if callable(widget) :
                    widget = widget()

                if widget :
                    widget.deleteLater()

            self.removeItem(item)
            del item

    def row(self, *widgets, align='Right') :

        hwgt = HWidget(self)

        if isinstance(align, (tuple, list) ) :
            qalign = getattr(Qt, 'Align'+align[0]) | getattr(Qt, 'Align'+align[1])
        else :
            qalign = getattr(Qt, 'Align'+align)

        for i, w in enumerate(widgets) :
            if isinstance(w, str) :
                hwgt.layout.addWidget(WidgetLabel(text=w, align=align))
            else :
                #w.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
                hwgt.layout.addWidget(w, align=qalign)
                '''
            if i < len(widgets)-1 :
                frame = Frame(hwgt.layout)
                frame.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
                #frame.setFixedWidth(20)
                '''
    def addWidget(self, widget, align=Qt.Alignment()) :
        spacing = self.property('Spacing')
        if spacing and self.count():
            super().addWidget(Frame(tag=spacing))

        super().addWidget(widget, align=align)

    def addLayout(self, layout) :
        spacing = self.property('Spacing')
        if spacing and self.count():
            super().addWidget(Frame(tag=spacing))

        super().addLayout(layout)


class HLayout(Layout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None, **kargs):
        super().__init__(parent, 'LeftToRight', spacing, margin, align, **kargs)

class VLayout(Layout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None, **kargs):
        super().__init__(parent, 'TopToBottom', spacing, margin, align, **kargs)


class StackedLayout(AbstractLayout, QStackedLayout):
    def __init__(self, parent=None, spacing=0, margin=0, align=None,
    mode = 'StackOne', **kargs) :

        QStackedLayout.__init__(self)
        AbstractLayout.__init__(self, parent, spacing, margin, align, props=kargs)

        self.setStackingMode( getattr(QStackedLayout, mode) )

    def addWidget(self, widget) :
        spacing = self.property('Spacing')
        if spacing and self.count():
            super().addWidget(Frame(tag=spacing))
        super().addWidget(widget)

    '''
    def sizeHint(self) :
        if self.currentWidget() :
            return self.currentWidget().sizeHint()
        else :
            return super().sizeHint()
    def minimumSizeHint(self) :
        if self.currentWidget() :
            return self.currentWidget().minimumSizeHint()
        else :
            return super().minimumSizeHint()
    '''

class FormLayout(AbstractLayout, QFormLayout) :
    def __init__(self, parent=None, align='Right', **kargs):
        QFormLayout.__init__(self)
        AbstractLayout.__init__(self, parent, props=kargs)

        self.setAlignment(Qt.AlignBottom )
        self.setLabelAlignment(getattr(Qt,'Align'+align)|Qt.AlignBottom )

    def row(self, text='', field=None) :
        label = TextLabel(text=text, Hmargin='normal')

        spacing = self.property('Spacing')
        if spacing and self.count():
            super().addRow(Frame(tag=spacing))

        field.display_name = False
        print(field, field.name)
        self.addRow(label, field)


class Widget(AbstractWidget, QWidget):
    def __init__(self, parent=None, title='', tag=None, props={}):
        QWidget.__init__(self)
        AbstractWidget.__init__(self, parent, tag, props)

        if title :
            self.setWindowTitle(title)

        '''
    def paintEvent(self, p) :
        opt = QStyleOption()
        opt.init(self)
        p = QPainter(self)
        self.style().drawPrimitive(QStyle.PE_Widget, opt, p, self)
        '''


class TabWidget(AbstractWidget, QTabWidget):
    def __init__(self, parent=None, tag=None, props={}):
        QTabWidget.__init__(self)
        AbstractWidget.__init__(self, parent, tag, props)


class Frame(AbstractWidget, QFrame):
    def __init__(self, parent=None, title='', tag=None, **kargs):

        QFrame.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, props=kargs)

        if title :
            self.setWindowTitle(title)

class HWidget(Frame):
    def __init__(self, parent=None, align=None, title='', tag=None, **kargs):
        super().__init__(parent, title=title, tag=tag, **kargs)
        self.layout = HLayout(self, align=align, Spacing=kargs.get('Spacing') )


class VWidget(Frame):
    def __init__(self, parent=None, align=None, title='',
        tag=None, **kargs):
        super().__init__(parent, title=title, tag=tag, **kargs)
        self.layout = VLayout(self, align=align, Spacing=kargs.get('Spacing') )

class FWidget(Frame) :
    def __init__(self, parent=None, title='', align='Right',
    tag=None, **kargs):
        super().__init__(parent, title=title, tag=tag, **kargs)
        self.layout = FormLayout(self, align=align, Spacing=kargs.get('Spacing') )

class StackedWidget(Frame):
    def __init__(self, parent=None, mode='StackOne', tag=None, **kargs):
        super().__init__(parent, tag=tag, **kargs)

        self.layout = StackedLayout(self, mode=mode, Spacing=kargs.get('Spacing'))


class Icon(QIcon) :
    def __init__(self, icon, ext='png'):
        if not icon :
            super().__init__()
            return

        icon_path = icon
        if isinstance(icon, str) :
            if ('\\' in icon or '/' in icon) : # it's a path
                icon_path = Path(icon)
            else :
                icon_path = ICON_DIR/icon

        if not icon_path.suffix :
            icon_path = icon_path.with_suffix('.'+ext)

        super().__init__(icon_path.as_posix())
'''
class Pixmap(QPixmap) :
    def __init__(self, image):
        image_path = image
        if isinstance(image, str) :
            if ('\\' in image or '/' in image) : # it's a path
                image_path = Path(image)
            else :
                icon_path = ICON_DIR/icon

        if not icon_path.suffix :
            icon_path = icon_path.with_suffix('.'+ext)

        super().__init__(icon_path.as_posix())
'''

class ScrollArea(AbstractWidget, QScrollArea):
    def __init__(self, parent=None, widget=None, tag=None, **kargs):
        QScrollArea.__init__(self)
        AbstractWidget.__init__(self, parent, tag, **kargs)

        self.setWidget(widget)
        self.setWidgetResizable(True)


class VScrollArea(ScrollArea) :
    def __init__(self, parent=None, widget=None, tag=None, **kargs):
        super().__init__(parent, widget, tag, **kargs)

        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.setWidgetResizable(True)


class WidgetLabel(QLabel) :
    def __init__(self, parent=None, text='', align='Right', expand=True, tag=None, **kargs) :
        kargs= {'Height':'normal', 'Hmargin':'normal', **kargs}
        QLabel.__init__(self, text)
        AbstractWidget.__init__(self, parent, tag=tag, props=kargs)

        self._text = text
        self.align = align

        self.setAlignment(getattr(Qt,'Align'+align)| Qt.AlignVCenter)
        self.setMinimumWidth(0)

        _expand = QSizePolicy.Expanding if expand else QSizePolicy.Fixed
        self.setSizePolicy(_expand, QSizePolicy.Fixed)

    def get_text_width(self, text) :
        metric = QFontMetrics(self.font())
        return metric.width(text)

    def resizeEvent(self, e) :
        super().resizeEvent(e)

        metric = QFontMetrics( self.font() )

        text_width = self.get_text_width(self._text)
        margins = self.contentsMargins().left() + self.contentsMargins().right()
        content_width = e.size().width() - margins

        self.setText( metric.elidedText(self._text, Qt.ElideMiddle, content_width) )

class TextLabel(AbstractWidget, QLabel):
    click = Signal()
    double_click = Signal()

    def __init__(self, parent=None, text = '', align='Left', expand=True,
    word_wrap = False, tag=None, **kargs):

        props= {'Height':'normal', **kargs}

        QLabel.__init__(self, text)
        AbstractWidget.__init__(self, parent, tag=tag, props=props)

        self.setIndent(1)
        self.setWordWrap(word_wrap)
        self.setAlignment(getattr(Qt,'Align'+align)| Qt.AlignVCenter)

        self._pos = QPointF(0,0)
        self.interval = QApplication.instance().doubleClickInterval()
        self._time = QTime()
        self._time.start()
        self.setSizePolicy(QSizePolicy.Expanding if expand else QSizePolicy.Fixed, QSizePolicy.Fixed)

        #self.setMinimumWidth(1)

    def mousePressEvent(self, e) :
        if e.button() == Qt.LeftButton :

            dist = QLineF(self._pos, e.pos()).length()
            if dist<2 and self._time.elapsed()<self.interval :
                self.double_click.emit()

        self._pos = e.pos()
        self._time.restart()

        super().mousePressEvent(e)

    def mouseReleaseEvent(self, e) :
        if e.button() == Qt.LeftButton :
            dist = QLineF(self._pos, e.pos()).length()
            if dist<2 :
                self.click.emit()

        super().mouseReleaseEvent(e)

'''
class Image(QImage) :
    def __init__(self, image, alpha, saturation = 100) :
        super().__init__(Path(image).as_posix())

        p = QPainter()
        p.begin(self)
        p.setCompositionMode(QPainter.CompositionMode_DestinationIn)
        p.fillRect(self.rect(), QColor(0, 0, 0, alpha))
        p.end()


class Icon(QIcon) :
    def __init__(self, icon_path) :
        super().__init__()

        normal_image = Image(icon_path, alpha=175)
        active_image = Image(icon_path, alpha=250)

        self.normal_pixmap = QPixmap().fromImage(normal_image)
        self.active_pixmap = QPixmap().fromImage(active_image)

        self.addPixmap(self.normal_pixmap, state=QIcon.Off)
        self.addPixmap(self.active_pixmap, state=QIcon.On)
'''

class ImageLabel(AbstractWidget, QLabel) :
    def __init__(self, parent=None, image='', tag=None, **kargs):
        QLabel.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, props=kargs)

        self.setScaledContents(True)

        pixmap = image
        if isinstance(image, str) :
            pixmap = QPixmap(image)
        elif isinstance(image, Path) :
            pixmap = QPixmap(image.as_posix() )
        elif isinstance(image, QImage) :
            pixmap = QPixmap.fromImage(image)

        if image :
            self.setPixmap(pixmap)


class SpinBox(AbstractWidget, QSpinBox) :
    def __init__(self, parent=None, value=0, min=0, max=100, step=1,
    tag=None, **kargs):
        props= {'Height':'normal', 'Rounded' : 'normal',
        'FontWeight':'light', **kargs}

        QSpinBox.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, props=props)

        self.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.setSingleStep(step)
        #self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setAlignment(Qt.AlignHCenter)

        self.setRange(min, max)
        self.setValue(value)

    def wheelEvent(self, e) :
        if self.hasFocus() :
            super().wheelEvent(e)
        else :
            e.ignore()

class DoubleSpinBox(AbstractWidget, QDoubleSpinBox) :
    def __init__(self, parent=None, value=0, min=0, max=100, decimals=2,
    step=0.01, tag=None, **kargs):
        props= {'Height':'normal', 'Rounded' : 'normal',
        'FontWeight':'light', **kargs}

        QDoubleSpinBox.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, props=props)

        self.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.setDecimals(decimals)
        self.setSingleStep(step)
        #self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setAlignment(Qt.AlignHCenter)

        self.setRange(min, max)
        self.setValue(value)

    def wheelEvent(self, e) :
        if self.hasFocus() :
            super().wheelEvent(e)
        else :
            e.ignore()


class LabeledWidget(HWidget) :
    def __init__(self, parent=None, name='', widget=None, tag=None, **kargs):
        super().__init__(parent, tag=tag, **kargs)

        self.layout.setSpacing(5)
        self.label = WidgetLabel(self, name)
        self.layout.addWidget(widget)


class CheckBox(AbstractWidget, QCheckBox) :
    def __init__(self, parent=None ,name = '', tag=None, **kargs):
        props= {'Height':'normal', **kargs}

        QCheckBox.__init__(self, name)
        AbstractWidget.__init__(self, parent, tag=tag, props=props)


class ComboBox(AbstractWidget, QComboBox) :
    def __init__(self, parent=None, items=None, tag=None, **kargs):
        props= {'Height':'normal', 'Rounded' : 'normal', **kargs}

        QComboBox.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, props=props)

        self.setSizeAdjustPolicy(QComboBox.AdjustToContents)

        self.items = items
        if items :
            self.addItems(items)

    def sizeHint(self) :
        size = super().sizeHint()
        size.setWidth(size.width()+2)
        return size

    def mousePressEvent(self, e) :
        super().mousePressEvent(e)
        e.ignore()
        #print(self.currentData(Qt.UserRole))


class PushButton(AbstractWidget, QPushButton) :
    clicked = Signal(Qt.KeyboardModifiers)
    value_changed = Signal()

    def __init__(self, parent=None, text='', icon=None, connect=None,
    cursor=None, tooltip='', tag=None, **kargs):

        props = {'Height' : 'normal', 'IconSize' : 'normal',
        'Rounded' : 'normal', **kargs}

        QPushButton.__init__(self, text=text)
        AbstractWidget.__init__(self, parent, cursor=cursor, tag=tag, props=props)

        self.setToolTip(tooltip)
        self.modifiers = Qt.KeyboardModifiers

        if connect :
            self.clicked.connect(connect)

        if icon :
            self.setIcon(Icon(icon))

            if text :
                self.setProperty('Rpadding', 'small')
            else :
                self.setProperty('Width', self.property('Height'))
            #self.setProperty('Vpadding', 'very_small')


        if text and not icon :
            self.setProperty('Hpadding', 'small')

    def mousePressEvent(self, e) :
        super().mousePressEvent(e)
        #self.setDown(True)
        self.modifiers = e.modifiers()


    def mouseReleaseEvent(self, e) :
        self.setDown(False)
        if self.underMouse() :
            self.clicked.emit(self.modifiers)



class Menu(QMenu) :
    def __init__(self, parent=None, tag=None, **kargs) :
        props = {'Background' : 'dark', **kargs}

        QMenu.__init__(self, parent)
        AbstractWidget.__init__(self, parent, tag=tag, props=props)



class WidgetAction(QWidgetAction) :
    def __init__(self, menu, icon=None, text='', connect=None) :
        super().__init__(menu)

        widget = PushButton(text=text, icon=icon)
        self.setDefaultWidget(widget)

        menu.addAction(self)

        if connect :
            widget.clicked.connect(connect)

        widget.clicked.connect(menu.close)


class LineEdit(AbstractWidget, QLineEdit) :
    value_changed = Signal(str)
    focus_out = Signal()
    def __init__(self, parent=None, text='', icon=None, clear_btn=False,
    connect=None, tag=None, **kargs):
        props= {'Height':'normal', 'Rounded' : 'normal',
        'FontWeight':'light', 'FontSize':'normal', **kargs}

        QLineEdit.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, props=props)

        self.setText(text)

        self.textChanged.connect(lambda x : self.value_changed.emit(x))

        self.layout = HLayout(self)
        if icon :
            self.icon_btn = PushButton(self, icon='search', tag= 'Transparent',
            Size='small', IconSize='small', Margin= 'very_small')
            self.layout.addStretch()

        if clear_btn :
            self.clear_btn = PushButton(self, icon='close', tag= 'Transparent',
            Size='Width', IconSize='very_small', Vmargin='small')
            self.clear_btn.setCursor(Qt.ArrowCursor)
            self.clear_btn.hide()
            self.clear_btn.clicked.connect(lambda : self.clear())

            self.textChanged.connect(self.on_text_edited)

        if connect :
            self.textChanged.connect(connect)
        #search_btn = PushButton(wgt, icon='search', Height='very_small', IconSize='small'})
    @property
    def value(self) :
        return self.text()

    def resizeEvent(self, e) :
        lmargin, rmargin = self.height()*0.15, self.height()*0.15
        if hasattr(self, 'icon_btn') :
            lmargin = self.height()*0.85
        if hasattr(self, 'clear_btn'):
            rmargin = self.height()*0.65

        self.setTextMargins(QMargins(lmargin,0,rmargin,0))

    def on_text_edited(self, text) :
        self.clear_btn.setVisible(bool(text))

    def mousePressEvent(self, e) :
        super().mousePressEvent(e)
        if not self.rect().contains(e.pos()) :
            w = QApplication.activeWindow()
            pos = self.mapTo(w, e.pos())
            if w.rect().contains(pos) :
                self.focus_out.emit()


class PlainTextEdit(AbstractWidget, QPlainTextEdit) :
    click = Signal()
    focus_out = Signal()

    def __init__(self, parent=None, tag=None, **kargs):
        props = {'Rounded': 'normal', **kargs}

        QPlainTextEdit.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, props=props)

        self._pos = QPointF(0,0)
        self.document().setDocumentMargin(0)

    def mousePressEvent(self, e) :
        super().mousePressEvent(e)
        if e.button() == Qt.LeftButton :
            self._pos = e.pos()
        if not self.rect().contains(e.pos()) :
            w = QApplication.activeWindow()
            pos = self.viewport().mapTo(w, e.pos())
            if w.rect().contains(pos) :
                self.focus_out.emit()

    def mouseReleaseEvent(self, e) :
        super().mouseReleaseEvent(e)
        if e.button() == Qt.LeftButton :
            dist = QLineF(self._pos, e.pos()).length()
            if dist<2 :
                self.click.emit()


class HSlider(AbstractWidget, QDoubleSpinBox) :
    clicked = Signal()

    def __init__( self, parent=None, name='', value=0, min=None, max=None,
        use_offset=False, decimals=2, step=0.1, unit='', tag=None, **kargs) :

        props= {'Height':'normal', 'Rounded' : 'normal',
        'FontWeight':'light', 'FontSize':'normal', **kargs}

        QDoubleSpinBox.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, props=props)

        self.min = min
        self.max = max
        self.step = step
        self.use_offset = use_offset
        self.unit = unit

        self.hover = False

        self._x = 0
        self._value = 0

        min = -float('inf') if self.min is None else self.min
        max = float('inf') if self.max is None else self.max

        self.setRange(min, max)
        self.setValue(value)
        self.setDecimals(decimals)

        self.lineEdit().hide()
        self.setButtonSymbols(QAbstractSpinBox.NoButtons)

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

        self.layout = StackedLayout(self, mode = 'StackAll')

        self.text_frame = Frame(self.layout)
        self.text_layout = HLayout(self.text_frame)

        #self.setProperty('BaseWidth', True)
        #self.setMinimumWidth(96)
        self.setMinimumWidth(1)
        #self.label_text.setGraphicsEffect(Shadow(self))

        if name :
            self.label_text = TextLabel(self.text_layout, name)
            self.text_layout.addStretch()

        self.label_value = TextLabel(self.text_layout)

        if not name :
            self.label_value.setAlignment(Qt.AlignCenter| Qt.AlignVCenter)
        #self.label_value.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        #self.label_value.setGraphicsEffect(Shadow(self))

        self.slider_frame = Frame(self.layout, tag = 'SliderFrame')
        #self.slider_frame.setGraphicsEffect(Shadow(self, color = 30, radius =4))

        #self.background = Frame(self.layout, tag = 'WidgetBackground')

        self.valueChanged.connect(self.update)

        self.update()
        #self.background.setProperty('ArrayIndex','first')

    def set_array_index(self,v) :
        self.setProperty('ArrayIndex',v)


    '''
    @property
    def name(self) :
        self.label_text.text()

    @name.setter
    def name(self, name):
        self.label_text.setText(name)
    '''
    def minimumSizeHint(self) :
        return QSize()

    def sizeHint(self) :
        return QSize()

    def update(self) :
        self.label_value.setText(self.lineEdit().text()+' '+self.unit)

        if self.min is None or self.max is None :
            w = 0
        else :
            pos = (self.value()-self.minimum()) / (self.maximum() - self.minimum())
            w = pos*self.width()

        self.slider_frame.setMask(QRegion(-1,0,w+1,self.height()))

    def resizeEvent(self, e) :
        super().resizeEvent(e)
        self.update()

    def mousePressEvent(self, event) :
        self._x = event.x()
        self._value = self.value()

    def mouseMoveEvent(self, event):
        offset = (event.x()-self._x)

        if self.min is not None and self.max is not None :
            pos = (offset/self.width()) * (self.maximum() - self.minimum())
        else :
            coeff = 1 if self.decimals() == 0 else 0.1
            pos = offset *coeff

        step_offset = (pos%self.step)
        if round(step_offset, self.decimals() ) != self.step :
            pos = pos - (pos%self.step)

        self.setValue(self._value + pos)
        self.valueChanged.emit(self.value())


    def mouseReleaseEvent(self, e) :
        super().mouseReleaseEvent(e)
        if e.button() == Qt.LeftButton :
            dist = QLineF(self._x, 0, e.x(), 0).length()
            if dist<2 :
                self.clicked.emit()

'''
class ListWidgetItem(CheckBox) :
    pressed = Signal()
    def __init__(self, list_widget, text = '', tag=None, props={}):
        self.props = {'FontWeight':'normal', 'FontSize':'normal',
        'Height' : 'normal', 'Rounded' : 'normal', **props}

        super().__init__(name=text, props=self.props)

        self.list_widget = list_widget
        self.pressed.connect(lambda : list_widget.setActiveWidget(self))

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

    def mousePressEvent(self, e) :
        if e.button() is Qt.LeftButton :
            self.pressed.emit()

    def hitButton(self, pos) :
        return True



class ListItemSmall(ListWidgetItem) :
    def __init__(self, list_widget, text = '', tag=None, props={}):
        self.props = {'FontWeight':'light', 'FontSize':'small',
        'Height' : 'very_small', **props}

        super().__init__(list_widget, text=text, tag=tag, props=self.props)


class ListWidget(Frame) :
    index_changed = Signal(int)
    def __init__(self, parent=None, items=None, orientation='vertical', tag=None, props={}):
        super().__init__(parent, tag, props)

        if orientation == 'vertical' :
            self.layout = VLayout(self)
            #self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        if orientation == 'horizontal' :
            self.layout = HLayout(self)
            self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)

        self.widgets = []
        self.layout.setAlignment(Qt.AlignTop)
        self.layout.setAlignment(Qt.AlignLeft)

        if items :
            for i in items :
                self.addWidget(i)

        self.setActiveIndex(0)

    def clear(self) :

        lyt = self.layout
        for i in reversed(range(lyt.count())):
            lyt.itemAt(i).widget().setParent(None)

        self.widgets = []

    def activeWidget(self) :
        return [w for w in self.widgets if w.isChecked()][0]

    def setActiveWidget(self, widget) :
        i = self.widgets.index(widget)
        self.setActiveIndex(i)

    def setActiveIndex(self, index) :
        for i, w in enumerate(self.widgets) :
            w.setChecked(index==i)

        self.index_changed.emit(index)

    def addWidget(self, text, props={}) :
        w = ListWidgetItem(self, text, props)
        self.widgets.append(w)
        self.layout.addWidget(w)
'''

class ComboBoxItem(PushButton) :
    def __init__(self, parent=None, icon='', text='', tooltip='') :
        super().__init__(parent=parent, icon=icon, text=text, tooltip=tooltip)

        #self.setObjectName('ComboBoxItem')
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.setCheckable(True)
        self.setFocusPolicy(Qt.NoFocus)
        self.text = text
        #self.modifier = ()

    def mousePressEvent(self, e) :
        super().mousePressEvent(e)
        self.modifier = e.modifiers()
        self.toggle()
        self.pressed.emit()
        #print(self.isChecked())


class ComboBoxExpand(Frame):
    currentTextChanged = Signal(str)
    def __init__(self, parent=None, items=[], orientation='horizontal', connect=None) :
        super().__init__(parent=parent)

        if orientation == 'horizontal' :
            self.layout = HLayout(self, Spacing='tiny')

        elif orientation == 'vertical' :
            self.layout = VLayout(self, Spacing='tiny')
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        #self.layout.setAlignment(Qt.AlignHCenter)
        self.items = []
        self.active_item = None

        for w in items :
            self.addItem(w)

        if connect :
            self.currentTextChanged.connect(connect)

    def item(self, name) :
        items = [i for i in self.items if i.text == name]
        if not items :
            print('The item', name, 'is not in', self.items)
            return
        return items[0]

    def currentText(self) :
        current_item = self.currentItem()
        return current_item.text if current_item else None

    def currentItem(self) :
        return next( (i for i in self.items if i.isChecked()), None)

    def addItem(self, item) :
        if isinstance(item, str) :
            item = ComboBoxItem(self, text=item)

        if not self.currentItem():
            self.setCurrentItem(item)

        item.pressed.connect(lambda : self.setCurrentItem(item))
        self.items.append(item)
        #self.layout.addWidget(item)

    def setCurrentItem(self, item) :
        if self.active_item :
            self.active_item.setChecked(False)

        self.active_item = item
        item.setChecked(True)

        self.currentTextChanged.emit(item.text)

    def setCurrentText(self, text) :
        self.setCurrentItem( self.item(text) )


class ListItem(QListWidgetItem) :
    def __init__(self, parent, widget, name, tag=None, **kargs) :
        super().__init__(parent, tag=tag, **kargs)

        self.name = name
        self.widget = widget
        self.setSizeHint(self.sizeHint() )
        parent.setItemWidget(self, widget)
        widget.item = self

        parent.addItem(self)


    def sizeHint(self) :
        return self.widget.sizeHint()
        #size = self.widget.sizeHint()
        #w, h = size.width(), size.height()

        #return QSize(w+200, h+200)

class ListWidget(QListWidget, AbstractWidget):
    def __init__(self, parent=None, extend=False, tag=None, **kargs):
        props = {**kargs}
        QListWidget.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, props=props)

        self.extend = extend
        if extend :
            self.setSelectionMode(QAbstractItemView.ExtendedSelection)

class VListWidget(ListWidget) :
    value_changed = Signal(str)

    def __init__(self, parent=None, tag=None, **kargs):
        kargs = { **kargs}
        super().__init__(parent, tag=tag, **kargs)

        self.setAlternatingRowColors(True)
        self.setSizeAdjustPolicy(QListWidget.AdjustToContents)

        self.currentItemChanged.connect(self.on_item_changed)

    def currentWidget(self) :
        return self.itemWidget(self.currentItem())

    def widgets(self) :
        items = [self.item(i) for i in range(self.count() )]
        widgets = [self.itemWidget(i) for i in items]
        return [w for w in widgets if w]

    def selected_widget(self) :
        widgets = [self.itemWidget(i) for i in self.selectedItems()]
        return [w for w in widgets if w]

    def addWidget(self, widget, name) :
        item = ListItem(self, widget, name)

        if not self.currentItem() :
            self.setCurrentItem(item)

        return item

    def on_item_changed(self, i, j) :
        item = i if i else j
        self.value_changed.emit(item.name)


class Dialog(QDialog, AbstractWidget) :
    def __init__(self, parent=None, size=None, title='', tag=None, icon=None,
    **kargs):
        props = {'Background':'dark', **kargs}

        QDialog.__init__(self, parent)
        AbstractWidget.__init__(self, tag=tag, props=props)

        if size :
            self.resize(*size)

        self.setWindowTitle(title)
        self.setWindowIcon(Icon(icon) )
        layout = VLayout(self)


class HDialog(Dialog) :
    def __init__(self, parent=None, size=None, title='', tag=None, icon=None,
    **kargs):
        super().__init__(parent, size=size, title=title, icon=icon,
        tag=None, **kargs)

        widget = HWidget(self, tag=tag, **kargs)
        self.layout = widget.layout


class VDialog(Dialog) :
    def __init__(self, parent=None, size=None, title='', tag=None, icon=None,
    **kargs):
        super().__init__(parent, size=size, title=title, icon=icon,
        tag=None, **kargs)

        widget = VWidget(self, tag=tag, **kargs)
        self.layout = widget.layout


class MainWindow(QMainWindow, AbstractWidget) :
    def __init__(self, parent=None, size=None, title='', tag=None, icon=None,
    **kargs):
        props = {'Background':'dark', **kargs}

        QMainWindow.__init__(self, parent)
        AbstractWidget.__init__(self)

        if size :
            self.resize(*size)

        self.setWindowTitle(title)
        self.setWindowIcon(Icon(icon) )
        self.setCentralWidget( VWidget(tag=tag, **props) )
        self.layout = self.centralWidget().layout
        #layout = VLayout(self)


class ListLabel(QScrollArea, AbstractWidget):
    def __init__(self, parent=None, items=[], align='Center', tag=None, **kargs):
        props = {'Background':'normal', 'Rounded':'normal', **kargs}

        QScrollArea.__init__(self)
        AbstractWidget.__init__(self, parent, tag=tag, props=props)

        self.widget = VWidget(align=('HCenter', 'Top'), Background='transparent', Margin='small')
        #self.widget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.setWidget(self.widget)
        self.setWidgetResizable(True)
        self.align = align

        self.items = []

        for i in items :
            self.addItem(i)


    def addItem(self, text) :
        label = TextLabel(self.widget, text=text, align= self.align,
        FontSize='normal', FontWeight='light', FontColor='disabled',
        Margin='very_small', Height='small')

        self.items.append(label)

        self.widget.updateGeometry()

        return label

    def clear(self) :
        lyt = self.widget.layout
        for i in reversed(range(lyt.count())):
            lyt.itemAt(i).widget().setParent(None)

        self.items = []
