
from PySide2.QtWidgets import QApplication, QListView, QListWidgetItem
from PySide2.QtCore import QSize, QCoreApplication
from PySide2.QtGui import QImage

import json
from pathlib import Path

from catalogue.constants import *
from catalogue.widgets.basic_widgets import *
from catalogue.widgets.properties import *
from catalogue.template import Template


class SearchThread(QThread) :
    item_found = Signal(dict)

    def __init__(self, parent, path):
        super().__init__()

        self.parent = parent
        self.path = path

    def run(self) :
        for d in self.path.iterdir() :
            if not d.is_dir() : continue

            info = d/f'{d.name}.json'

            if not info.exists() :
                continue

            data = { 'name' : d.name, **json.loads(info.read_text() ) }
            data['image'] = QImage( (d/data['thumbnail']).as_posix() )
            data['root'] = d
            tpl = Template((d/data['template']).as_posix())

            fields = {}
            for item in tpl.find_all({}) :
                item_data = tpl.parse(item)
                for k, v in item_data.items() :
                    if k not in fields :
                        fields[k] = []

                    if v not in fields[k] :
                        fields[k].append(v)
                        fields[k].sort()

            data['fields'] = fields
            #return
            #d.glob(data[])
            self.item_found.emit( data )

            #QCoreApplication.processEvents()

class ThumbnailWidget(VWidget) :
    def __init__(self, parent, data):
        super().__init__()

        self.image = ImageLabel(self, data['image'])
        self.name = data['name']
        self.parent = parent
        self.data = data

        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

        #self.image_layout = VLayout(self.image)
        #self.image_layout.addStretch()
        self.label = WidgetLabel(self, self.name.replace('_', ' ').title(),
        align='Center', tag='Stamp', Height='small')
        self.label.setFixedHeight(12)
        #self.label.setIndent(0)
        self.parent.addWidget(self, self.name)

        #print(self.sizeHint())

    def sizeHint(self) :
        pix = self.image.pixmap()
        ratio = pix.height() / pix.width()
        zoom = self.parent.zoom

        #print(QSize(zoom*150, zoom*150*ratio))
        return QSize(zoom*150, zoom*150*ratio+12 )


class ThumbnailList(VListWidget) :
    def __init__(self, parent=None):
        super().__init__(parent, Background='very_dark', tag='Thumbnail')

        app = QApplication.instance()

        self.setViewMode(QListView.IconMode)
        self.setResizeMode(QListView.Adjust)
        #self.setWrapping(True)
        #self.setIconSize(QSize(150, 150))
        self.setMovement(QListView.Static)
        self.setSpacing(5)

        self.zoom = 1
        self.search_thread = None
        #self.search_thread.item_found.connect(self.add_item )
        #self.search_thread.start()
        #app.signals.started.connect(self.set_items)
        #self.set_items()
        #self.set_items
        self.currentItemChanged.connect(self.on_current_item_changed)
        #self.itemDoubleClicked.connect(self.load_item)


    def on_current_item_changed(self, item):
        if item and item.widget :
            APP.signals.item_changed.emit(item.widget.data )
            APP.item = item

    def load_item(self, item=None) :
        if not APP.server.connections :
            print('No Blender connected')
            return

        if not item :
            item = APP.item

        settings= {k:v.value for k, v in APP.options.items()}

        data = item.widget.data

        possibilities = []
        for field_name, values in settings.items() :
            for v in values :
                possibility ={field_name:v}
                for other_field_name, other_values in settings.items() :
                    if other_field_name == field_name : continue

                    possibility = possibility.copy()
                    for other_v in other_values :
                        possibility.update({other_field_name:other_v})

                if possibility not in possibilities :
                    possibilities.append(possibility)

        assets = [Template(data['template']).format(d) for d in possibilities]
        assets = [ str(data['root']/a) for a in assets]

        APP.server.connections[-1].send(['POST', [{'type': data['type'], 'files' : assets}]])

    def filter(self, name) :
        for w in self.widgets() :
            w.item.setHidden( not any([name in i for i in w.data['tags']+[w.name] ]) )

    def wheelEvent(self, event) :
        if Qt.ControlModifier == event.modifiers() :
            self.zoom = sorted((0.5, self.zoom + event.delta()/1200, 3))[1]

            for i in [self.item(i) for i in range(self.count() )] :
                i.setSizeHint(i.widget.sizeHint())
                if not i.isHidden() :
                    i.setHidden(True)
                    i.setHidden(False)

        else :
            super().wheelEvent(event)


    def add_item(self, data) :
        widget = ThumbnailWidget(self, data)


    def set_items(self, path) :
        print('set_items',path)
        if self.search_thread and self.search_thread.isRunning() :
            self.search_thread.terminate()
            self.search_thread.wait()

        self.search_thread = SearchThread(self, path)
        self.search_thread.item_found.connect(self.add_item )
        self.search_thread.start()
