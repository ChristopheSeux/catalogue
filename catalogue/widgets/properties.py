from PySide2.QtWidgets import *
from PySide2.QtGui import *
from PySide2.QtCore import *

from catalogue.widgets.basic_widgets import *
from catalogue.base_objects.stylesheet import StyleSheet
from math import ceil


class BaseNumber(HWidget) :
    def __init__(self, value=0, min=0, max=100, decimals=2, step=0.01,
    reset=False, parent=None, name='', display_name=True, tag=None, **kargs) :

        kargs = {'Height':'normal', 'Rounded' : 'normal', **kargs}

        super().__init__(parent, tag=tag, align='Right', Spacing='tiny')

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

        self.hover = False
        self.reset = reset
        self.default_value = value
        self.min = min
        self.max = max

        self.label = WidgetLabel(self, name)
        self.name = name
        self.display_name = display_name

        if decimals == 0 :
            min = -2147483648 if self.min is None else self.min
            max = 2147483647 if self.max is None else self.max
            self.spinbox = SpinBox(self, value=value, min=min, max=max, step=step)
        else :
            min = -float('inf') if self.min is None else self.min
            max = float('inf') if self.max is None else self.max
            self.spinbox = DoubleSpinBox(self, value=value, min=min, max=max, step=step,
            decimals=decimals)

        self.spinbox.setFocusPolicy( Qt.StrongFocus )
        self.spinbox.layout = HLayout(self.spinbox)
        Frame(self.spinbox.layout, tag='very_small')

        self.reset_btn = PushButton(self.spinbox.layout, icon='reset_32', tag='Transparent', IconSize='small',
        connect=lambda : setattr(self, 'value',self.default_value) )
        self.reset_btn.setFocusPolicy(Qt.NoFocus)
        self.reset_btn.hide()

        self.spinbox.layout.addStretch()

        self.left_btn = PushButton(self.spinbox.layout, icon='down_32', tag='Transparent', Size='very_small', IconSize='small',
        connect=lambda : self.spinbox.setValue(self.value-self.spinbox.singleStep()) )
        self.left_btn.setFocusPolicy(Qt.NoFocus)
        self.left_btn.hide()

        self.right_btn = PushButton(self.spinbox.layout, icon='up_32', tag='Transparent', Size='very_small', IconSize='small',
        connect=lambda : self.spinbox.setValue(self.value+self.spinbox.singleStep()) )
        self.right_btn.setFocusPolicy(Qt.NoFocus)
        self.right_btn.hide()

        Frame(self.spinbox.layout, tag='very_small')
        Frame(self.spinbox.layout, tag='tiny')

        if reset :
            self.spinbox.valueChanged.connect(self.set_reset_btn )

    def enterEvent(self, e) :
        super().enterEvent(e)
        self.hover = True
        self.set_reset_btn()
        self.right_btn.show()
        self.left_btn.show()

    def leaveEvent(self, e) :
        super().leaveEvent(e)
        self.hover = False
        self.set_reset_btn()
        self.right_btn.hide()
        self.left_btn.hide()

    def set_reset_btn(self) :
        self.reset_btn.setVisible(self.reset and self.value != self.default_value)


    @property
    def display_name(self):
        return self.label.isVisible()
    @display_name.setter
    def display_name(self, value):
        self.label.setVisible(value)

    @property
    def name(self):
        return self.label.text()
    @name.setter
    def name(self, value):
        self.label.setText(value)

    @property
    def value(self):
        return self.spinbox.value()
    @value.setter
    def value(self, value):
        self.spinbox.setValue(value)


class Int(BaseNumber) :
    value_changed = Signal(int)

    def __init__(self, value=0, min=0, max=100, decimals=2, step=1,
    reset=False, name='', parent=None, tag=None, **kargs) :

        super().__init__(value=value, min=min, max=max, decimals=0, step=step,
        reset=reset, name=name, parent=parent, tag=tag, **kargs)

        self.spinbox.valueChanged.connect(self.value_changed)


class Float(BaseNumber) :
    value_changed = Signal(float)

    def __init__(self, value=0, min=0, max=100, decimals=2, step=1,
    reset=False, name='', parent=None, tag=None, **kargs) :

        super().__init__(value=value, min=min, max=max, decimals=decimals, step=step,
        reset=reset, name=name, parent=parent, tag=tag, **kargs)

        self.spinbox.valueChanged.connect(self.value_changed)

'''
class Number(HWidget) :
    value_changed = Signal(float)
    def __init__(self, value=0, parent = None, name='', min=None, max=None,
    step = 0.01, unit='', decimals=2, type='spinbox', **kargs) :

        kargs = {'Height':'normal', 'Rounded' : 'normal', **kargs}

        super().__init__(parent, **kargs)

        self.slider = HSlider(self, name=name, value=value, min=min, max=max,
        step=step, unit=unit, decimals=decimals)

        self.slider.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.slider.setFocusPolicy(Qt.NoFocus)

        self.slider.clicked.connect(self.edit_value)

        self.line_edit = LineEdit()
        self.line_edit.setSizePolicy(self.sizePolicy())
        self.line_edit.editingFinished.connect(self.editing_finished)

        self.layout.addWidget(self.slider)
        self.layout.addWidget(self.line_edit)

        self.line_edit.setVisible(False)

        self.slider.valueChanged.connect(self.on_value_changed)

    def on_value_changed(self, value) :
        self.value_changed.emit(value)

    def edit_value(self) :
        self.line_edit.canceled = False
        self.line_edit.setText(str(self.slider.value()))
        self.line_edit.setVisible(True)
        self.line_edit.selectAll()
        self.line_edit.setFocus()
        self.slider.setVisible(False)

        #print(QApplication.focusWidget())

    def editing_finished(self) :
        #print(self.line_edit.canceled)
        if not self.line_edit.canceled :
            try :
                value = eval(self.line_edit.text())
            except :
                value = None
                print('Wrong Value')

            if isinstance(value,(float, int)) :
                self.value = value

        #print(QApplication.focusWidget())
        self.line_edit.clearFocus()
        self.line_edit.setVisible(False)
        self.slider.setVisible(True)

    @property
    def value(self):
        return self.slider.value()

    @value.setter
    def value(self, value):
        self.slider.setValue(value)

    @property
    def name(self):
        return self.slider.name

    @name.setter
    def name(self, text):
        self.slider.name = text



    def edit_start(self) :
        if self.stack_layout.currentWidget() is self.line_edit : return

        self.stack_layout.setCurrentWidget(self.line_edit)
        self.line_edit.setText(str(self.slider.value()))
        self.line_edit.setFocus()
        self.line_edit.grabMouse()
        self.line_edit.selectAll()
        self.line_edit.setCursor(Qt.IBeamCursor)


    def edit_finish(self) :
        if self.stack_layout.currentWidget() is self.slider : return

        self.stack_layout.setCurrentWidget(self.slider)
        self.line_edit.setCursor(Qt.ArrowCursor)
        self.line_edit.releaseMouse()
        self.line_edit.clearFocus()

        self.line_edit.hide()

        try :
            value = eval(self.line_edit.text())
            self.slider.setValue(value)
            print('Emit')
            self.value_changed.emit(value)
        except :
            value = None

    @property
    def value(self):
        return self.slider.value()
    @value.setter
    def value(self, value):
        self.slider.setValue(value)

    @property
    def name(self, name) :
        return self.slider.name
    @name.setter
    def name(self, name) :
        self.slider.name = name

    def set_array_index(self, v) :
        self.slider.set_array_index(v)
        self.line_edit.set_array_index(v)

'''

class Color(HWidget):
    value_changed= Signal(float)

    def __init__(self, r=240, g=240, b=240, a=255, parent=None, name='', **kargs):
        super().__init__(parent)

        self._border_radius = 3.0
        self._border_width = 1
        self._inset_strength = 0.15
        self._text_color = QColor(230,230,230)
        self._value = (r,g,b,a)

        self.setProperty('Height', 'normal')
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

        self.label = WidgetLabel(self.layout, name)
        self.color_btn = PushButton(self.layout, tag = 'ColorPropBtn')
        self.color_btn.setMinimumWidth(1)

        stylesheet = '#ColorPropBtn {background : rgba[color]}'
        #self.btn_stylesheet = StyleSheet(stylesheet, prop_groups=({'color':self},))

    @property
    def value(self) :
        return self._value

    @value.setter
    def value(self, value) :
        self._value = value
        self.value_changed.emit()


class Text(PlainTextEdit) :
    value_changed= Signal(str)

    def __init__(self, value = '', parent = None, **kargs):
        super().__init__(parent, **kargs)

        self.value = value
        #self._edit = True

        # Connections
        #self.click.connect(self.edit_start)
        #self.focus_out.connect(self.edit_finish)

        #self.viewport().mousePressEvent = self.mousePressEvent
        #self.viewport().mouseReleaseEvent = self.mouseReleaseEvent

        #self.edit_finish()

        #self.setGraphicsEffect(Shadow(self, color = 30, radius =2, offset = 2))

    def deselectAll(self) :
        cursor = self.textCursor()
        cursor.clearSelection()
        self.setTextCursor(cursor)

        '''
    def edit_start(self) :
        print('edit start')
        if self._edit : return

        self.setFocusPolicy(Qt.StrongFocus)
        self.setTextInteractionFlags(Qt.TextEditorInteraction)
        self.viewport().grabMouse()
        self.viewport().setCursor(Qt.IBeamCursor)
        self.setFocus()
        self.selectAll()
        self._edit = True

        self.verticalScrollBar().hide()

    def edit_finish(self) :
        if not self._edit : return

        self.viewport().setCursor(Qt.ArrowCursor)
        self.viewport().releaseMouse()
        self.clearFocus()
        self.setTextInteractionFlags(Qt.NoTextInteraction)
        self.setFocusPolicy(Qt.NoFocus)
        self.deselectAll()
        self._edit = False

        self.verticalScrollBar().show()
        QApplication.sendEvent(self, QEvent(QEvent.Leave) )

        self.value_changed.emit(self.toPlainText())
        '''
    @property
    def value(self):
        return self.toPlainText()

    @value.setter
    def value(self, value):
        self.setPlainText(value)


class Filepath(HWidget) :
    def __init__(self, value='', name='', display_name=True, parent=None, align='Right', tag=None, props={}):
        super().__init__(parent, tag=tag, props=props)

        self.label = WidgetLabel(self, text=name, align=align, expand=align=='Right')
        self.name = name
        self.display_name = display_name

        self.line_edit = LineEdit(self)
        self.file_dlg = QFileDialog(self)
        self.open_file_dlg = PushButton(self, icon='folder', tag='Transparent',
        IconSize='small', Hmargin= 'very_small')
        #self.open_file_dlg
        self.open_file_dlg.clicked.connect(lambda : self.file_dlg.exec())
        self.file_dlg.fileSelected.connect(lambda x : self.line_edit.setText(x) )

        if value :
            self.value = value

    @property
    def display_name(self):
        return self.label.isVisible()
    @display_name.setter
    def display_name(self, value):
        self.label.setVisible(value)

    @property
    def name(self):
        return self.label.text()
    @name.setter
    def name(self, value):
        self.label.setText(value)

    @property
    def value(self):
        return self.line_edit.text()
    @value.setter
    def value(self, value):
        self.line_edit.setText(value)


class String(LineEdit) :
    value_changed = Signal(str)

    def __init__(self, value='', parent=None, tag=None, **kargs):
        super().__init__(parent, text=value, tag=tag, **kargs)

    @property
    def value(self):
        return self.text()

    @value.setter
    def value(self, value):
        self.setText(value)

'''
class String(StackedWidget) :
    value_changed = Signal(str)

    def __init__(self, value='', parent=None, tag=None, **kargs):
        super().__init__(parent, tag=tag, **kargs)

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.setObjectName('label')

        self.label = TextLabel(self, value, tag='Editable')
        self.line_edit = LineEdit(self)
        self.line_edit.setSizePolicy(self.sizePolicy())
        #self.label.setGraphicsEffect( Shadow(self) )

        self.value = value
        self.is_edited = False

        # Connections
        self.label.click.connect(self.edit_start)
        self.line_edit.editingFinished.connect(self.edit_finish)
        self.line_edit.focus_out.connect(self.edit_finish)
        self.line_edit.textChanged.connect(lambda t : self.label.setText(t))

        #self.setGraphicsEffect(Shadow(self, color = 30, radius =2, offset = 2))
        #self.edit_finish()

    def mousePressEvent(self,e) :
        if not self.rect().contains(e.pos()) :
            self.edit_finish()

    def edit_start(self) :
        print('Edit Start')
        if self.is_edited :
            return

        self.is_edited = True
        if self.layout.currentWidget() is self.line_edit : return

        self.layout.setCurrentWidget(self.line_edit)
        self.line_edit.selectAll()
        self.line_edit.setFocus()

        self.line_edit.grabMouse()

        self.line_edit.setCursor(Qt.IBeamCursor)

    def edit_finish(self) :
        if not self.is_edited :
            return

        print('finished')
        self.is_edited = False
        self.layout.setCurrentWidget(self.label)
        self.line_edit.setCursor(Qt.ArrowCursor)

        self.line_edit.releaseMouse()
        self.line_edit.clearFocus()

        self.value_changed.emit(self.line_edit.text())

    @property
    def value(self):
        return self.line_edit.text()

    @value.setter
    def value(self, value):
        self.line_edit.setText(value)
'''

class Enum(ComboBox):
    value_changed = Signal(str)
    def __init__(self, value='', items=None, connect=None, parent = None, tag=None, **kargs):
        super().__init__(parent, items, tag=tag, **kargs)
        self.value = value

        view = QListView()
        self.setView(view)

        self.currentTextChanged.connect(lambda x : self.value_changed.emit(x))

        if connect :
            self.value_changed.connect(connect)

    @property
    def value(self):
        return self.currentText()

    @value.setter
    def value(self, value):
        #print('old_value :', self.value, 'new_value :', value)
        if value == self.value :
            self.value_changed.emit(value)
        self.setCurrentText(value)


class ListEnum(ListWidget) :
    value_changed = Signal(str)
    def __init__(self, parent=None, items=[], extend=False, expand=True, tag=None, **kargs):
        kargs = {'Height':'normal', **kargs}
        super().__init__(parent, extend=extend, tag='ListEnum', **kargs)

        #self.index_changed.connect(lambda x : self.value_changed.emit(self.widgets[x].text()) )
        #self.setMinimumWidth(0)
        self.setFlow(QListView.LeftToRight)
        self.setSizePolicy(QSizePolicy.Expanding if expand else QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)


        #self.verticalScrollBar().setEnabled(False)
        #self.horizontalScrollBar().setEnabled(False)
        #self.sert


        #self.setMinimumWidth(1)
        #self.setResizeMode(QListView.Adjust)
        #self.setSizeAdjustPolicy(QListWidget.AdjustToContents)
        #self.viewport().setMinimumWidth(1)
        #self.viewport().setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        for i in items :
            self.addItem(i)

        #self.setMinimumWidth(self.sizeHintForColumn(0))
        self.currentItemChanged.connect(lambda : self.value_changed.emit(self.value))


    def addItem(self, text) :
        item = QListWidgetItem(self, text = text)
        item.text = text
        item_widget = HWidget(Hmargin='small')
        widget = TextLabel(item_widget, text=text, align='Center', tag='ListWidgetItem')
        widget.setIndent(0)

        #widget.setObjectName('ListEnum')
        #item_widget.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        #print(item_widget.rect(), item_widget.contentsRect(),item_widget.sizeHint())
        w, h = item_widget.sizeHint().width()+3, widget.sizeHint().height()
        item.setSizeHint(QSize(w ,h))
        self.setItemWidget(item, item_widget)
        super().addItem(item)

        if not self.currentItem() :
            self.setCurrentItem(item)

        self.updateGeometry()

        #self.setFixedWidth(self.sizeHintForColumn(0))

    #def sizeHint(self) :

    def sizeHint(self) :
        size = super().minimumSizeHint()
        width =  [self.item(i).sizeHint().width() for i in range(self.count())]
        return QSize(sum(width), size.height())


    '''
    def preferred_width(self) :
        return sum( [self.itemWidget(self.item(i)).sizeHint().width()+3 \
                    for i in range(self.count() )])

    def minimumSizeHint(self) :
        size = super().minimumSizeHint()
        if self.sizePolicy().horizontalPolicy() == QSizePolicy.Expanding :
            return size

        w = self.preferred_width()
        return QSize(w+ 2 * self.frameWidth(), size.height())

    def resizeEvent(self, e) :
        super().resizeEvent(e)
        if self.count() and self.sizePolicy().horizontalPolicy() == QSizePolicy.Expanding:
            #print('Resize')
            w, h = e.size().width(), e.size().height()
            ratio = w/self.preferred_width()
            for i in range(self.count() ):
                item = self.item(i)

                size = QSize(ratio *(self.itemWidget(item).sizeHint().width()+3)-1 , h)
                item.setSizeHint(size)
    '''

    @property
    def value(self):
        if self.extend :
            return [i.text for i in self.selectedItems()]
            #return [self.item(i).text for i in range(self.count() ) if item.isSelected()]
        else :
            item = self.currentItem()
            if item :
                return item.text

    @value.setter
    def value(self, value):
        if not isinstance(value, list) :
            value = [value]

        items = [self.item(i) for i in range(self.count() )]

        for i in items :
            i.setSelected(False)

        for v in value :
            item = [i for i in items if i.text==v]
            if item :
                item[0].setSelected(True)


class Bool(CheckBox):
    value_changed = Signal(int)
    def __init__(self, value=False, parent=None, name='', **kargs):
        kargs = {'Rounded': 'normal', 'IconSize':'small', **kargs}

        super().__init__(parent, name = name, **kargs)

        self.value = value

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        #self.setAlignment(Qt.AlignRight)
        self.setLayoutDirection(Qt.RightToLeft)
        self.stateChanged.connect(lambda x : self.value_changed.emit(x))

        #self.setGraphicsEffect(Shadow(self, color = 30, radius =2, offset = 2))

    def mousePressEvent(self, event) :
        if self.hitButton(event.pos()) :
            self.setChecked(not self.isChecked())

    @property
    def value(self):
        return self.isChecked()

    @value.setter
    def value(self, value):
        self.setChecked(value)
