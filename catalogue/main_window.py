
from PySide2.QtWidgets import QApplication, QSizePolicy, QLabel
from PySide2.QtCore import QSettings, Signal, QObject, Qt

from pathlib import Path

from catalogue.server import ServerThread
from catalogue.constants import MODULE_DIR, ICON_DIR, STYLESHEET, CONFIG, APP
from catalogue.base_objects.stylesheet import StyleSheet
from catalogue.preferences import Preferences
from catalogue.widgets.basic_widgets import MainWindow, PushButton, HWidget, VWidget,\
LineEdit, Frame, HLayout, TextLabel, WidgetLabel
from catalogue.widgets.custom_widgets import ThumbnailList
from catalogue.widgets.properties import Enum, ListEnum
from catalogue.windows.prefs_dlg import PrefsDlg


class Signals(QObject) :
    closed = Signal()
    started = Signal()
    connection_updated = Signal(object)
    item_changed = Signal(dict)

class MainWindow(MainWindow) :
    def __init__(self, parent=None):
        super().__init__(title='Catalogue', icon='kitsu')

        self.resize(510, 540)

        APP.item = None
        APP.settings = QSettings('settings', 'catalogue')
        APP.signals = Signals()
        APP.preferences = Preferences()
        prefs = APP.preferences

        self.start_server()

        ## Apply Dynamic Stylesheet
        constants = {'ICON_DIR' : ICON_DIR.as_posix()}
        widgets = {**prefs.display.to_dict(), **prefs.theme.to_dict() }
        APP.stylesheet = StyleSheet(STYLESHEET, constants, widgets)

        self.prefs_dlg = PrefsDlg(self)

        self.header = VWidget(self, Spacing='small', Padding='small', background='dark')

        ## Search Bar
        self.search_bar = HWidget(self.header, Background='dark')

        self.type_enum = Enum(parent=self.search_bar)
        self.read_config()
        #self.type_enum.value_changed.connect(self.on_type_changed)

        self.search_ledit = LineEdit(self.search_bar, icon='search', clear_btn=True,
        connect= self.filter)
        self.refresh_btn = PushButton(self.search_bar, icon='refresh',
        tag='Transparent',cursor='PointingHandCursor')
        frame = Frame(self.search_bar, tag='small')
        self.settings_btn = PushButton(self.search_bar, icon='settings',
        tag='Transparent', cursor='PointingHandCursor', connect=lambda : self.prefs_dlg.show() )

        ## Option Bar
        self.options_bar = HWidget(self.header, Background='dark')
        APP.options = {}
        #self.options = {}


        Frame(self, Background='background', tag='tiny')

        self.icon_list = ThumbnailList(self)

        self.type_enum.value_changed.connect(self.on_type_enum_changed)
        self.type_enum.value_changed.emit('')

        APP.signals.item_changed.connect(self.set_fields)

    def set_option(self, field, value) :
        print('set options', field, value)
        APP.options[field] = value

    def set_fields(self, data) :
        self.options_bar.layout.clear()
        settings= {k:v.value for k, v in APP.options.items()}

        APP.options = {}
        #self.options_bar.layout = HLayout(self.options_bar)
        field_widget = HWidget(self.options_bar)
        for field_name, values in data['fields'].items() :
            label = TextLabel(parent=field_widget, text=field_name.title()+' : ', expand=False)
            field = ListEnum( parent= field_widget, items=[v for v in values],
            expand=False, extend=True)

            if field_name in settings :
                field.value = settings[field_name]

            APP.options[field_name] = field

            Frame(field_widget, tag='normal')
        self.options_bar.layout.addStretch()

        self.load_btn = PushButton(parent=self.options_bar, text='Load',
        connect=self.icon_list.load_item)

        #        print(data)

    def start_server(self) :
        ## Launch Catalogue Server
        APP.server = ServerThread()
        APP.server.start()

    def filter(self, name) :
        self.icon_list.filter(name)

    def on_type_enum_changed(self) :
        self.icon_list.clear()
        self.icon_list.set_items(self.type_enum.currentData(Qt.UserRole))

        self.search_ledit.clear()

    def read_config(self) :
        self.type_enum.clear()
        for i, f in enumerate(CONFIG['folders']) :
            path = Path(f)
            self.type_enum.addItem(path.name.replace('_', '').title())
            self.type_enum.setItemData(i, path, role=Qt.UserRole)

    def showEvent(self, event) :
        APP.signals.started.emit()

    def restore_settings(self) :
        return
        index = APP.settings.value('active_tab', None)
        if index is None : index = 0
        self.tabs.setCurrentIndex(int(index))

        project = APP.settings.value('project' , None)
        if project is None : project = APP.projects[0].name
        self.project.value = project

    def closeEvent(self, event):
        APP.signals.closed.emit()

        #self.save_settings()
        super().closeEvent(event)

    def save_settings(self):
        return
        APP.settings.setValue('active_tab', self.tabs.currentIndex() )
        APP.settings.setValue('project', self.project.value )
