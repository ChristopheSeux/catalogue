import socket
import pickle
from PySide2.QtCore import QThread
from PySide2.QtWidgets import QApplication

from catalogue.constants import APP

import socket
import threading


#Client class, new instance created for each connected client
#Each instance has the socket and address that is associated with items
#Along with an assigned ID and a name chosen by the client
class Client(QThread):
    def __init__(self, server, client, address):
        super().__init__()
        self.client = client
        self.address = address
        self.buffer_size = 4096

        self.signal = True
        self.server = server

        self.filepath = None
        self.version = None
        self.data = {}
        self.entity = None
        self.versions = []

        self.send(['GET', {'filepath': 'bpy.data.filepath'}])

    def __str__(self):
        return str(self.address)

    def receive(self) :
        msg = b''
        while self.signal :
            try :
                print('[SERVER] Waiting for client input')
                chunk = self.client.recv(self.buffer_size)
            except Exception as e :
                #print(e)
                self.close()
                break

            if chunk == b'' :
                self.close()
                break

            msg+= chunk
            if not chunk or len(chunk) < self.buffer_size: break

        if msg :
            return pickle.loads(msg)

    def close(self) :
        print(f'[SERVER] The server is closing the connection with client {self.address}')
        self.signal = False
        self.client.shutdown(2)
        self.client.close()
        self.filepath = None
        self.server.connections.remove(self)
        APP.signals.connection_updated.emit(self)

    def send(self, msg) :
        d = pickle.dumps(msg)
        self.client.sendall(d)

        print('[SERVER] Message sent', msg)

    def run(self):
        while self.signal:
            msg = self.receive()

            if not msg : break

            print('[SERVER] Data Receive from client :', msg)

            if msg == 'Exit' :
                self.close()

            elif isinstance(msg, (list, tuple)) and len(msg) == 2 :
                action, data = msg

                if action == 'POST' :
                    self.data.update(data)

                    #if 'filepath' in data  :
                    #    path = data['filepath']
                    #    self.send(['POST', data])

                    APP.signals.connection_updated.emit(self)


class ServerThread(QThread) :
    def __init__(self) :
        super().__init__()
        #Variables for holding information about connections
        self.connections = []

        print('Start Server Thread')
        #Get host and port
        self.host = socket.gethostname()
        self.port = 9999

        #Create new server socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #try :
        self.sock.bind((self.host, self.port))
        self.sock.listen(5)

        signals = QApplication.instance().signals

        signals.closed.connect(self.close)

    def close(self) :
        for c in self.connections :
            c.close()

        #self.sock.shutdown(2)
        self.sock.close()
        self.isRunning = False

    #Wait for new connections
    def run(self):
        while self.isRunning :
            try :
                sock, address = self.sock.accept()
            except :
                break

            client = Client(self, sock, address)
            self.connections.append(client)
            client.start()
            #QApplication.instance().signal.connection_updated.emit()

            print("[SERVER] New connection " + str(client))

        print('[SERVER] Server Thread terminate')
