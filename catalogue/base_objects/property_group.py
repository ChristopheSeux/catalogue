from PySide2.QtCore import Signal

from catalogue.widgets.properties import *


class PropertyGroup() :
    def __init__(self, **kargs) :
        super().__init__()
        for k,v in kargs.items() :
            super().__setattr__(k, v)

    def __getattribute__(self, k) :
        if hasattr(super().__getattribute__(k), 'value') :
            return super().__getattribute__(k).value
        else :
            return super().__getattribute__(k)

    def __setattr__(self, k, v) :
        if hasattr(v, 'value') or isinstance(v, PropertyGroup) :
            super().__setattr__(k, v)
        else :
            super().__getattribute__(k).value = v

    def __getitem__(self, k):
        return super().__getattribute__(k)

    def __setitem__(self, key, value):
        self._data[key].value = value

    def items(self) :
        return self.__dict__.items()

    def keys(self) :
        return self.__dict__.keys()

    def values(self) :
        return self.__dict__.values()

    def to_dict(self) :
        return self.__dict__

    def dict(self) :
        return {k:getattr(self,k) for k in self.keys()}




        '''
        # Set Fonts
        font_db = QFontDatabase()
        for f in os.scandir(FONT_PATH) :
            font_id = font_db.addApplicationFont(f.path)
            family = QFontDatabase.applicationFontFamilies(font_id)
            print(f.name, family)
        '''
