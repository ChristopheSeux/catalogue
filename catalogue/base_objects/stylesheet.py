
from PySide2.QtWidgets import QApplication
from PySide2.QtGui import QColor

import yaml
from functools import partial
import re
from pathlib import Path


class StyleSheet :
    def __init__(self, stylesheet, constants={}, widgets={}) :
        if Path(stylesheet).exists() :
            self.stylesheet = Path(stylesheet).read_text()
        else :
            self.stylesheet = stylesheet

        self.constants = dict(constants, lighten=self.lighten, darken=self.darken)
        self.widgets = widgets

        for k, v in widgets.items() :
            v.value_changed.connect(self.update)

        self.pattern = re.compile('\[([^=]*?)\]')
        self.fields = [(e.groups()[0], e.group()) for e in self.pattern.finditer(self.stylesheet)]

        self.update()


    def lighten(self, color, ratio=0.5):
        c = color
        if not isinstance(color, QColor) :
            c = QColor(*color)

        c = c.lighter(100+ ratio*100)

        if isinstance(color, QColor) :
            return c
        else :
            return (c.red(), c.green(), c.blue(), c.alpha() )

    def darken(self, color, ratio=0.5):
        c = color
        if not isinstance(color, QColor) :
            c = QColor(*color)

        c = c.darker(100+ ratio*200)

        if isinstance(color, QColor) :
            return c
        else :
            return (c.red(), c.green(), c.blue(), c.alpha() )

    def update(self) :
        app = QApplication.instance()
        css = self.stylesheet

        attrs = dict(self.constants, **{k:v.value for k,v in self.widgets.items()} )

        for result, match in self.fields :
            v = eval(result, attrs)
            css = css.replace(match, str(v),1)

        app.setStyleSheet(css)
