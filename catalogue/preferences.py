
from catalogue.base_objects.property_group import PropertyGroup
from catalogue.widgets.properties import *

class Preferences(PropertyGroup) :
    def __init__(self) :
        super().__init__()

        self.general = PropertyGroup(
            ui_scale = Float(value=1.0, name='UI Scale', min=0.5, max=5, step=0.05),
            config = Filepath(value='', name='Config', align='Right'),
        )

        self.display = PropertyGroup(
            ui_scale = Float(value=1.0, name='UI Scale', min=0.5, max=5, step=0.05),
            brightness = Float(value=0.5, name='Brightness')
        )

        self.theme = PropertyGroup(
            background_color = Color(34, 36, 39, name='Background Color'),
            widget_very_dark_color = Color(44, 46, 51, name='Widget Very Dark Color'),
            widget_dark_color = Color(54, 57, 63, name='Widget Dark Color'),
            widget_base_color = Color(78, 81, 89, name='Widget Base Color'),
            widget_bright_color = Color(110, 115, 118, name='Widget Base Color'),
            text_color_disabled = Color(170, 170, 170, name='Text Color Disabled'),
            text_color_normal = Color(190, 190, 190, name='Text Color Normal'),
            text_color_highlight = Color(240, 240, 240, name='Text Color Highlight'),
            select_text_bg = Color(30, 35, 40, name='Selected Text Background'),
            border_radius = Int(1, name='Border Radius',min=0, max=10, decimals=1, unit='px'),
            border_width = Int(1, name = 'Border Width',min=0, max=10, decimals=0, unit='px'),
            highlight_color = Color(85, 127, 193, name='Highlight Color'),
        )
