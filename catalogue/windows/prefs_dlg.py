from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import QApplication

from catalogue.widgets.basic_widgets import *
from catalogue.widgets.properties import *


class PrefsDlg(HDialog) :
    def __init__(self, parent=None):
        super().__init__(parent, title='Preferences', Background='very_dark')

        app = QApplication.instance()
        self.resize(360, 480)

        app.signals.closed.connect(self.save_settings)

        self.tabs = TabWidget(self)

        # General Tab
        self.general_tab = VWidget( title='General', align='Top',
        Background='dark')

        fwidget = FWidget(self.general_tab, Spacing='small', Padding='normal')
        for k, v in app.preferences.general.items() :
            fwidget.layout.row(v.name, v)

        for t in ( self.general_tab, ) :
            self.tabs.addTab(ScrollArea(widget=t), t.windowTitle() )


        #self.restore_settings()

    def restore_settings(self) :
        return
        local_root = self.app.project.settings.value('local_root', None)
        if local_root is not None:
            self.local_root.value = local_root

        work_in_local = self.app.settings.value('work_in_local', None)
        if work_in_local is not None :
            self.work_in_local.value = bool(work_in_local)

    def save_settings(self):
        return
        self.app.project.settings.setValue('local_root', self.local_root.value )
        self.app.project.settings.setValue('work_in_local', int(self.work_in_local.value)  )
