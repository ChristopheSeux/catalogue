import sys
from PySide2.QtWidgets import QApplication,QMainWindow
from PySide2.QtCore import Qt

import os

if __name__ == '__main__':
    app = QApplication(sys.argv)

    from catalogue.main_window import MainWindow
    window = MainWindow()
    window.show()

    app.exec_()
